<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* orders/show.html.twig */
class __TwigTemplate_5fa8d5e660d4f28fde502454a1102a326d7d9b66a1f4ea010a84fc02ff9d881d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "orders/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "orders/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "orders/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h2>Order ID: ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 4, $this->source); })()), 0, [], "any", false, false, false, 4), "orderid", [], "any", false, false, false, 4), "html", null, true);
        echo "</h2>
    <hr>
    <em>Phone: ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 6, $this->source); })()), 0, [], "any", false, false, false, 6), "phone", [], "any", false, false, false, 6), "html", null, true);
        echo "</em><br />
    <em>Payment Status: ";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 7, $this->source); })()), 0, [], "any", false, false, false, 7), "paymentstatus", [], "any", false, false, false, 7), "html", null, true);
        echo "</em><br />
    <hr>
    <em>Shipping Status: ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 9, $this->source); })()), 0, [], "any", false, false, false, 9), "shippingstatus", [], "any", false, false, false, 9), "html", null, true);
        echo "</em><br />
    <em>Shipping Price: ";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 10, $this->source); })()), 0, [], "any", false, false, false, 10), "shippingprice", [], "any", false, false, false, 10), "html", null, true);
        echo "</em><br />
    <em>Shipping Payment Status: ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["orders"]) || array_key_exists("orders", $context) ? $context["orders"] : (function () { throw new RuntimeError('Variable "orders" does not exist.', 11, $this->source); })()), 0, [], "any", false, false, false, 11), "shippingpaymentstatus", [], "any", false, false, false, 11), "html", null, true);
        echo "</em><br />
    <hr>
    ";
        // line 13
        if (1 === twig_compare(twig_length_filter($this->env, (isset($context["items"]) || array_key_exists("items", $context) ? $context["items"] : (function () { throw new RuntimeError('Variable "items" does not exist.', 13, $this->source); })())), 0)) {
            // line 14
            echo "    <h4>Order items:</h4>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <td>Item Id</td>            
                <td>Price</td> 
                <td>Cost</td> 
                <td>Tax Perc</td> 
                <td>Tax Amt</td> 
                <td>Quantity</td>       
                <td>Tracking Nr</td>
                <td>Canceled</td>       
                <td>SKU</td>
            </tr>
        </thead>
        <tbody>
        ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) || array_key_exists("items", $context) ? $context["items"] : (function () { throw new RuntimeError('Variable "items" does not exist.', 30, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 31
                echo "            <tr>
                <td>";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "itemsid", [], "any", false, false, false, 32), "html", null, true);
                echo "</td> 
                <td>";
                // line 33
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "price", [], "any", false, false, false, 33), "html", null, true);
                echo "</td>
                <td>";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "cost", [], "any", false, false, false, 34), "html", null, true);
                echo "</td>
                <td>";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "taxperc", [], "any", false, false, false, 35), "html", null, true);
                echo "</td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "taxamt", [], "any", false, false, false, 36), "html", null, true);
                echo "</td>
                <td>";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 37), "html", null, true);
                echo "</td>
                <td>";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "trackingnumber", [], "any", false, false, false, 38), "html", null, true);
                echo "</td>
                <td>";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "canceled", [], "any", false, false, false, 39), "html", null, true);
                echo "</td>
                <td>";
                // line 40
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "shippedstatussku", [], "any", false, false, false, 40), "html", null, true);
                echo "</td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "    </ul>
    ";
        }
        // line 45
        echo "    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "orders/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 45,  164 => 43,  155 => 40,  151 => 39,  147 => 38,  143 => 37,  139 => 36,  135 => 35,  131 => 34,  127 => 33,  123 => 32,  120 => 31,  116 => 30,  98 => 14,  96 => 13,  91 => 11,  87 => 10,  83 => 9,  78 => 7,  74 => 6,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <h2>Order ID: {{ orders.0.orderid }}</h2>
    <hr>
    <em>Phone: {{ orders.0.phone }}</em><br />
    <em>Payment Status: {{ orders.0.paymentstatus }}</em><br />
    <hr>
    <em>Shipping Status: {{ orders.0.shippingstatus }}</em><br />
    <em>Shipping Price: {{ orders.0.shippingprice }}</em><br />
    <em>Shipping Payment Status: {{ orders.0.shippingpaymentstatus }}</em><br />
    <hr>
    {% if items|length > 0 %}
    <h4>Order items:</h4>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <td>Item Id</td>            
                <td>Price</td> 
                <td>Cost</td> 
                <td>Tax Perc</td> 
                <td>Tax Amt</td> 
                <td>Quantity</td>       
                <td>Tracking Nr</td>
                <td>Canceled</td>       
                <td>SKU</td>
            </tr>
        </thead>
        <tbody>
        {% for item in items %}
            <tr>
                <td>{{ item.itemsid }}</td> 
                <td>{{ item.price }}</td>
                <td>{{ item.cost }}</td>
                <td>{{ item.taxperc }}</td>
                <td>{{ item.taxamt }}</td>
                <td>{{ item.quantity }}</td>
                <td>{{ item.trackingnumber }}</td>
                <td>{{ item.canceled }}</td>
                <td>{{ item.shippedstatussku }}</td>
            </tr>
        {% endfor %}
    </ul>
    {% endif %}
    
{% endblock %}
", "orders/show.html.twig", "/home/ale_dv/Develop/pp_test/templates/orders/show.html.twig");
    }
}
