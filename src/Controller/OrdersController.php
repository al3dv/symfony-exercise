<?php

namespace App\Controller;

use App\Entity\Orders;
use App\Form\OrderType;
use App\Services\FileUploader;
use App\Repository\OrdersRepository;
use App\Repository\OrderItemsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/orders", name="orders.")
 */
class OrdersController extends AbstractController
{
    /**
     * @Route("/returndata", name="returndata")
     * @param OrdersRepository $orderRepository
     * @return Response
     */
    public function returndata(OrdersRepository $orderRepository)
    {
        $orders = $orderRepository->findAll();

        return $this->render('orders/return.html.twig', [
            'orders' => $orders
        ]);

    }

    /**
     * @Route("/show/{id}", name="show")
     * @param Post $orders
     * @return Response
     */
    public function show($id, OrdersRepository $orderRepository, OrderItemsRepository $orderItemsRepository) {
        $orders = $orderRepository->findOrderById($id);
        $items = $orderItemsRepository->findItemsByPostId($id);

        return $this->render('orders/show.html.twig', [
            'orders' => $orders,
            'items' => $items
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param OrdersRepository $orderRepository
     * @param OrderItemsRepository $orderItemsRepository
     */
    public function remove ($id, OrdersRepository $orderRepository, OrderItemsRepository $orderItemsRepository) {
        $orderItemsRepository->deleteItemsById($id);
        $orderRepository->deleteItemsById($id);

        $this->addFlash('success','Order was removed');

        return $this->redirect($this->generateUrl('orders.returndata'));
    }
}
