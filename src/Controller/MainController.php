<?php

namespace App\Controller;
use App\Services\GetExternalData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class MainController extends AbstractController
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/", name="show_json_dump")
     */
    public function layout(GetExternalData $getExternalData)
    {
        $url = $this->params->get('url_json');

        $content = $getExternalData->retreiveData($url);

        return $this->render('home/index.html.twig', [
            'content' => $content
        ]);
    }

    /**
     * @Route("/import_data", name="import_data")
     */
    public function import_data(GetExternalData $getExternalData)
    {
        $url = $this->params->get('url_json');
        $getExternalData->saveData($url);
        $this->addFlash('success','Data imported');
        return $this->redirect($this->generateUrl('show_json_dump'));
    }

}
