<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OrderItemsController extends AbstractController
{
    /**
     * @Route("/orders/items", name="order_items")
     */
    public function index()
    {
        return $this->render('order_items/index.html.twig', [
            'controller_name' => 'OrderItemsController',
        ]);
    }
}
