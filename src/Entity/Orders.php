<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $order_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItems",mappedBy="orders", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $items;

    /**  
     * @ORM\Column(type="string", length=100, nullable=true)
     */ 
    private $phone;

    /**  
     * @ORM\Column(type="string", length=100, nullable=true)
     */ 
    private $shipping_status;

    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
     private $shipping_price;

    /**  
     * @ORM\Column(type="string", length=100, nullable=true)
     */ 
    private $shipping_payment_status;

    /**  
     * @ORM\Column(type="string", length=100, nullable=true)
     */ 
    private $payment_status;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderId(): ?string
    {
        return $this->order_id;
    }

    public function setOrderId(?string $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getShippingStatus(): ?string
    {
        return $this->shipping_status;
    }

    public function setShippingStatus(?string $shipping_status): self
    {
        $this->shipping_status = $shipping_status;

        return $this;
    }

    public function getShippingPrice(): ?int
    {
        return $this->shipping_price;
    }

    public function setShippingPrice(?int $shipping_price): self
    {
        $this->shipping_price = $shipping_price;

        return $this;
    }

    public function getShippingPaymentStatus(): ?string
    {
        return $this->shipping_payment_status;
    }

    public function setShippingPaymentStatus(?string $shipping_payment_status): self
    {
        $this->shipping_payment_status = $shipping_payment_status;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->payment_status;
    }

    public function setPaymentStatus(?string $payment_status): self
    {
        $this->payment_status = $payment_status;

        return $this;
    }

    /**
     * @return Collection|OrderItems[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(OrderItems $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setPost($this);
        }

        return $this;
    }

    public function removeItem(OrderItems $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getPost() === $this) {
                $item->setPost(null);
            }
        }

        return $this;
    }

}
