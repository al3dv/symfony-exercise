<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemsRepository")
 */
class OrderItems
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $items_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Orders", inversedBy="orderitems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orders;

    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
    private $price;
    
    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
    private $cost;
    
    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
    private $tax_perc;
    
    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
    private $tax_amt;
    /**  
     * @ORM\Column(type="integer", nullable=true)
     */ 
    private $quantity;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tracking_number;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $canceled;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shipped_status_sku;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemsId(): ?string
    {
        return $this->items_id;
    }

    public function setItemsId(string $items_id): self
    {
        $this->items_id = $items_id;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getTaxPerc(): ?int
    {
        return $this->tax_perc;
    }

    public function setTaxPerc(?int $tax_perc): self
    {
        $this->tax_perc = $tax_perc;

        return $this;
    }

    public function getTaxAmt(): ?int
    {
        return $this->tax_amt;
    }

    public function setTaxAmt(?int $tax_amt): self
    {
        $this->tax_amt = $tax_amt;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(?string $tracking_number): self
    {
        $this->tracking_number = $tracking_number;

        return $this;
    }

    public function getCanceled(): ?string
    {
        return $this->canceled;
    }

    public function setCanceled(?string $canceled): self
    {
        $this->canceled = $canceled;

        return $this;
    }

    public function getShippedStatusSku(): ?string
    {
        return $this->shipped_status_sku;
    }

    public function setShippedStatusSku(?string $shipped_status_sku): self
    {
        $this->shipped_status_sku = $shipped_status_sku;

        return $this;
    }

    public function getOrder(): ?Orders
    {
        return $this->orders;
    }

    public function setOrder(?Orders $orders): self
    {
        $this->orders = $orders;

        return $this;
    }

    public function getOrders(): ?Orders
    {
        return $this->orders;
    }

    public function setOrders(?Orders $orders): self
    {
        $this->orders = $orders;

        return $this;
    }

                 
}
