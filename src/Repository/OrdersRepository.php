<?php

namespace App\Repository;

use App\Entity\Orders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    public function findOrderByCategory(int $id){

        $qb = $this->createQueryBuilder('p');
        $qb->select('p.title')
            ->addSelect('p.id as order_id')
            ->addSelect('c.name')
            ->addSelect('c.id as category_id')
            ->innerJoin('p.category', 'c')
            ->where('p.id = :id')
            ->setParameter('id', $id)
        ;

        return $qb->getQuery()->getResult();

    }

    public function findOrderById(int $id){

        $qb = $this->createQueryBuilder('p');
        $qb->andWhere('p.id = :id')
            ->setParameter('id', $id)
        ;

        return $qb->getQuery()->getResult();

    }

    public function deleteItemsById(int $id){
        $builder = $this->createQueryBuilder('c');
        $builder->delete()
                ->where('c.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->execute();
        
    }
    // /**
    //  * @return Orders[] Returns an array of Orders objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Orders
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
