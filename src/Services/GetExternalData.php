<?php

namespace App\Services;

use App\Entity\Orders;
use App\Entity\OrderItems;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PropertyAccess\PropertyAccess;

class GetExternalData extends AbstractController {

    public function retreiveData(String $url){

        try {
            $client = HttpClient::create();
            $response = $client->request('GET', $url);
            $statusCode = $response->getStatusCode();
            $contentType = $response->getHeaders()['content-type'][0];
            $content = $response->toArray();
        } catch (\InvalidArgumentException $ex) {
            error_log($ex->getMessage());
            $content = null;
        }

        return $content;
    }

    public function saveData(String $url)
    {


        $content = $this->retreiveData($url);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if (is_array($content) || is_object($content)) {
            foreach ($content as $data_item) {
                $entity = new Orders();
                $orderId = $propertyAccessor->getValue($data_item, '[orderId]');
                $phone = $propertyAccessor->getValue($data_item, '[phone]');
                $shipping_status = $propertyAccessor->getValue($data_item, '[shipping_status]');
                $shipping_price = $propertyAccessor->getValue($data_item, '[shipping_price]');
                $shipping_payment_status = $propertyAccessor->getValue($data_item, '[shipping_payment_status]');
                $payment_status = $propertyAccessor->getValue($data_item, '[payment_status]');

                $entity->setOrderId($orderId);
                $entity->setPhone($phone);
                $entity->setShippingStatus($shipping_status);
                $entity->setShippingPrice($shipping_price);
                $entity->setShippingPaymentStatus($shipping_payment_status);
                $entity->setPaymentStatus($payment_status);

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $orderItems = $propertyAccessor->getValue($data_item, '[orderItems]');
                foreach ($orderItems as $order_item) {
                    $orderItem = new OrderItems();
                    $items_id = $propertyAccessor->getValue($order_item, '[item_sid]');

                    $price = $propertyAccessor->getValue($order_item, '[price]');
                    $cost = $propertyAccessor->getValue($order_item, '[cost]');
                    $tax_perc = $propertyAccessor->getValue($order_item, '[tax_perc]');
                    $tax_amt = $propertyAccessor->getValue($order_item, '[tax_amt]');
                    $quantity = $propertyAccessor->getValue($order_item, '[quantity]');
                    $tracking_number = $propertyAccessor->getValue($order_item, '[tracking_number]');
                    $canceled = $propertyAccessor->getValue($order_item, '[canceled]');
                    $shipped_status_sku = $propertyAccessor->getValue($order_item, '[shipped_status_sku]');

                    $orderItem->setItemsId($items_id);
                    $orderItem->setOrder($entity);

                    $orderItem->setPrice($price);
                    $orderItem->setCost($cost);
                    $orderItem->setTaxPerc($tax_perc);
                    $orderItem->setTaxAmt($tax_amt);
                    $orderItem->setQuantity($quantity);
                    $orderItem->setTrackingNumber($tracking_number);
                    $orderItem->setCanceled($canceled);
                    $orderItem->setShippedStatusSku($shipped_status_sku);


                    $em_oi = $this->getDoctrine()->getManager();
                    $em_oi->persist($orderItem);
                    $em_oi->flush();
                }
            }
        }
    }
}    