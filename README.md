
## How To Run

1. Download the **Source**  
   *git clone https://al3dv@bitbucket.org/al3dv/symfony-exercise.git*
2. Modify **.env** and set your environment paramenters for DB [postgres](https://www.postgresql.org/) 
3. Create entity into your environment  
   *bin/console doctrine:schema:update --force*
4. Run the **Server**  
   *php bin/console server:run*